package com.abbvie.data;

/**
 * Created by irit on 28/11/2016.
 */
public class Patient {

    private String patientName;
    private String patientPhone;
    private String patientPhonePrefix;

    public Patient(String patientName, String patientPhone)
    {
        this(patientName, patientPhone, null);
    }

    public Patient(String patientName, String patientPhone, String patientPhonePrefix) {
        this.patientName = patientName;
        this.patientPhone = patientPhone;
        this.patientPhonePrefix = patientPhonePrefix;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientPhone() {
        return patientPhone;
    }

    public void setPatientPhone(String patientPhone) {
        this.patientPhone = patientPhone;
    }

    public String getPatientPhonePrefix() {
        return patientPhonePrefix;
    }

    public void setPatientPhonePrefix(String patientPhonePrefix) {
        this.patientPhonePrefix = patientPhonePrefix;
    }
}
