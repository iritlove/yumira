package com.abbvie.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.abbvie.data.Patient;
import com.abbvie.R;


public class SendMailTask extends AsyncTask<String, String, Boolean> {
	
	private Activity contextActivity;
	ProgressDialog mProgressDialog;
	Patient registeredPatient;

	
	public SendMailTask(Activity contextAct, Patient registeredPatient)
				
	{
		this.registeredPatient = registeredPatient;
		this.contextActivity = contextAct;
		//this.myApp = globalApp;
		mProgressDialog = new ProgressDialog(contextActivity);//, R.style.ProgressBarStyle);
		mProgressDialog.setTitle(R.string.sendingMail);
		mProgressDialog.setCancelable(false);

		
	}
	

	
	
	@Override
    protected void onPreExecute() {
    	mProgressDialog.show();
    	super.onPreExecute();
    }
	// method will receive 2 strings: first email address second email message
	@Override
	protected Boolean doInBackground(String... params) {
		return MailsUtil.sendMail(contextActivity, params[0],params[1], params[2], registeredPatient);
		
	}
	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		if(result)
		{
			mProgressDialog.dismiss();
			UIUtils.showCustomDialog( contextActivity, R.string.mailSentSuccessfully, true);
		}
		else
		{
			mProgressDialog.dismiss();
			UIUtils.showCustomDialog( contextActivity, R.string.mailNotSent, false);
		}
	}
	

	
	
	


}
