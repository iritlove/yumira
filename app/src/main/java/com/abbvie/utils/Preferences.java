package com.abbvie.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


public class Preferences {
	private static final String KEY = "abbvie-session";
	/***/
	
	
	public static final String PREFERENCE_DR_NAME = "PREFERENCE_DR_NAME";
	public static final String PREFERENCE_DR_PHONE = "PREFERENCE_DR_PHONE";
	public static final String PREFERENCE_MEDICAL_CENTER = "PREFERENCE_MEDICAL_CENTER";

	
	
	public static SharedPreferences restorePreferences(Context context) {
		SharedPreferences savedSession = context.getSharedPreferences(KEY, Context.MODE_PRIVATE);
		return savedSession;
	}

	public static void clear(Context context) {
		Editor editor = context.getSharedPreferences(KEY, Context.MODE_PRIVATE).edit();
		editor.clear();
		editor.commit();
	}

	

	/**
	 * @param context
	 * @return
	 */
	public static String getDrName(Context context) {
		SharedPreferences prefs = context.getSharedPreferences(KEY, Context.MODE_PRIVATE);
		return prefs.getString(PREFERENCE_DR_NAME, "");
	}

	/**
	 * @param context
	 * @param drName
	 */
	public static void setDrName(Context context, String drName) {
		Editor editor = context.getSharedPreferences(KEY, Context.MODE_PRIVATE).edit();
		editor.putString(PREFERENCE_DR_NAME, drName);
		editor.commit();
	}
	
	/**
	 * @param context
	 * @return
	 */
	public static String getDrPhone(Context context) {
		SharedPreferences prefs = context.getSharedPreferences(KEY, Context.MODE_PRIVATE);
		return prefs.getString(PREFERENCE_DR_PHONE, "");
	}

	/**
	 * @param context
	 * @param drPhone
	 */
	public static void setDrPhone(Context context, String drPhone) {
		Editor editor = context.getSharedPreferences(KEY, Context.MODE_PRIVATE).edit();
		editor.putString(PREFERENCE_DR_PHONE, drPhone);
		editor.commit();
	}
	
	/**
	 * @param context
	 * @return
	 */
	public static String getMedicalCenter(Context context) {
		SharedPreferences prefs = context.getSharedPreferences(KEY, Context.MODE_PRIVATE);
		return prefs.getString(PREFERENCE_MEDICAL_CENTER, "");
	}

	/**
	 * @param context
	 * @param medicalCenter
	 */
	public static void setMedicalCenter(Context context, String medicalCenter) {
		Editor editor = context.getSharedPreferences(KEY, Context.MODE_PRIVATE).edit();
		editor.putString(PREFERENCE_MEDICAL_CENTER, medicalCenter);
		editor.commit();
	}
	
	

	

}