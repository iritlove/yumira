package com.abbvie.utils;



import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.abbvie.R;


public class UIUtils {


	/* Name of doctor, Name of patient and name of medical center cannot contain
	characters like *&^%$, only letters or -
	 */
	public static boolean isNameValid(String name)
	{
		if(isEmpty(name))
			return false;

		for(int i=0;i<name.length();i++) {
			boolean validConditions = Character.isLetter(name.charAt(i)) ||
										name.charAt(i) == '-' ||
										name.charAt(i) == '\"' ||
										name.charAt(i) == '\'' ||
										Character.isSpaceChar(name.charAt(i));
			if (!validConditions)
				return false;
		}

		return true;
	}


	public static boolean isPhoneValid(String phone)
	{
		if(isEmpty(phone))
			return false;
		if(phone.length() != 10)
			return false;
		if(!phone.startsWith("05"))
			return false;
		for(int i=0;i<phone.length();i++) {
			if (!Character.isDigit(phone.charAt(i)))
				return false;
		}

		return true;
	}

	public static boolean isOnline(Context context) {
		ConnectivityManager cm =
				(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		return netInfo != null && netInfo.isConnectedOrConnecting();
	}


	public static boolean isEmpty(String text)
	{
		if(text == null || text.equals(""))
			return true;
		return false;
	}

	public static void showCustomDialog(final Activity contextActivity, String messageString, final boolean isFinish)
	{
		final Dialog dialog = new Dialog(contextActivity);
		dialog.setContentView(R.layout.custom_dialog_layout);
		dialog.setTitle(contextActivity.getString(R.string.alertTitle));

		int textViewId = dialog.getContext().getResources().getIdentifier("android:id/title", null, null);
		TextView tv = (TextView) dialog.findViewById(textViewId);

		if(tv != null)
		{
			tv.setGravity(Gravity.RIGHT|Gravity.CENTER_VERTICAL);
			tv.setTextColor(contextActivity.getResources().getColor(R.color.buttonBgColor));
		}




		// set the custom dialog components - text, image and button
		TextView text = (TextView) dialog.findViewById(R.id.messageText);
		text.setText(messageString);


		TextView dialogButton = (TextView) dialog.findViewById(R.id.okButton);
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				if(isFinish)
					contextActivity.finish();

			}
		});

		dialog.show();
	}

	public static void showCustomDialog(final Activity contextActivity, int stringResId, final boolean isFinish)
	{
		showCustomDialog(contextActivity, contextActivity.getString(stringResId), isFinish);
	}

	

	

}
