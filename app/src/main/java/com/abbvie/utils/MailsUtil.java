package com.abbvie.utils;

import java.util.Properties;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.abbvie.data.Patient;
import com.abbvie.R;


public class MailsUtil
{


	public static String fromUserName = "humira.app@gmail.com";
	public static String fromPassword = "humiraapp";

	//public static String careForLifeEmail = "amir.ariel@abbvie.com";
	//public static String careForLifeEmail = "irit.lovenberg@gmail.com";
	public static String careForLifeEmail = "callcenter@care4life.co.il";

	public static final String TAG = "TestMails";
	
	private static Properties SMTPprops = new Properties();
	private static Properties IMAPprops = new Properties();
	static
	{
		SMTPprops.put("mail.smtp.auth", "true");
		SMTPprops.put("mail.smtp.starttls.enable", "true");
		SMTPprops.put("mail.smtp.host", "smtp.gmail.com");
		SMTPprops.put("mail.smtp.port", "587");

		IMAPprops.setProperty("mail.imaps.host", "imap.gmail.com");
		IMAPprops.setProperty("mail.imaps.port", "993");
		IMAPprops.setProperty("mail.imaps.connectiontimeout", "5000");
		IMAPprops.setProperty("mail.imaps.timeout", "5000");

	}


    public static boolean sendMail(Context context, final String fromEmail, final String fromPassword,
								   String toEmail, Patient registeredPatient)
    {
    	
        Session session = Session.getInstance(SMTPprops,
        new javax.mail.Authenticator(){
            protected PasswordAuthentication getPasswordAuthentication()
            {
                return new PasswordAuthentication(fromEmail, fromPassword);
            }
        });
        try
        {
        	// first mail is to the Admin - order details
            Message message = new MimeMessage(session);
            message = constructMail(context, message,
            		new InternetAddress(fromEmail),
            		toEmail, registeredPatient);
            Transport.send(message);

			// TODO - should we wait a few seconds before deletion?

			new DeleteSentItemsTask().execute();

       }

        catch (Exception e) 
        {
        	e.printStackTrace();
        	return false;
        }
        return true;

    }
    
   
    
    public static Message constructMail(Context context, Message message,
    		InternetAddress fromAddress, String toAddress, Patient registeredPatient)
    {
    	String messageStr = "";
	   	 try
	   	 {
				message.setFrom(fromAddress);
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toAddress));
				String messageBody = "<body style='direction:rtl' align='right'>";
				messageBody = messageBody + context.getString(R.string.MailCN_patientName) + " " + registeredPatient.getPatientName()
						+ "<br/>";
				messageBody = messageBody + context.getString(R.string.MailCN_patientPhone) + " " + registeredPatient.getPatientPhone() + "<br/>";

				messageBody = messageBody + context.getString(R.string.MailCN_mdPhone) + " " + Preferences.getDrPhone(context) + "<br/>";
			 	messageBody = messageBody + context.getString(R.string.MailCN_mdName) + " " + Preferences.getDrName(context) + "<br/>";
				messageBody = messageBody + context.getString(R.string.MailCN_medicalCenter) + " " + Preferences.getMedicalCenter(context) + "<br/>";
			 	messageBody = messageBody + "</body>";

				message.setSubject(context.getString(R.string.MailCN_title));
				messageStr += messageBody;

				message.setContent(messageStr, "text/html" );
		         
	   	 }catch(Exception e)
	   	 {
	   		 e.printStackTrace();
	   		 return null;
	   	 }
    	return message;
    	
    }

	static class DeleteSentItemsTask extends AsyncTask<Void, Void, Boolean>
	{

		@Override
		protected Boolean doInBackground(Void... params) {
			Session session = Session.getInstance(IMAPprops,
					new javax.mail.Authenticator(){
						protected PasswordAuthentication getPasswordAuthentication()
						{
							return new PasswordAuthentication(fromUserName, fromPassword);
						}
					});



			try {
				Store store = session.getStore("imaps");
				store.connect(fromUserName, fromPassword);


				Folder folderSent = store.getFolder("[Gmail]/Sent Mail");
				Folder folderTrash = store.getFolder("[Gmail]/Trash");
				folderTrash.open(Folder.READ_WRITE);
				folderSent.open(Folder.READ_WRITE);

				Message[] arrayMessages = folderSent.getMessages();
				folderSent.copyMessages(arrayMessages, folderTrash);

				Message[] trashMessages = folderTrash.getMessages();

				for (int i = 0; i < trashMessages.length; i++) {
					Message trashMessage = trashMessages[i];
					String subject = trashMessage.getSubject();
					trashMessage.setFlag(Flags.Flag.DELETED, true);

					Log.e(TAG, "Marked DELETE for message: " + subject);
				}
				folderTrash.expunge();



				/*Message[] trashMessages = folderTrash.getMessages();
				for (int i = 0; i < trashMessages.length; i++) {
					Message trashMessage = trashMessages[i];
					String subject = trashMessage.getSubject();
					trashMessage.setFlag(Flags.Flag.DELETED, true);

					Log.e(TAG, "Marked DELETE in trash for message: " + subject);
				}
				folderTrash.expunge();*/

				folderSent.close(true);
				folderTrash.close(true);

				store.close();
			}catch(MessagingException e)
			{
				Log.e(TAG, "Exception " + e.getMessage() + " in mail delete");
				return false;
			}
			return true;
		}
	}


    
}