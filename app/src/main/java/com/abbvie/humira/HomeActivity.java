package com.abbvie.humira;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.abbvie.R;
import com.abbvie.utils.Preferences;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HomeActivity extends AppCompatActivity {

    Button exitKioskModeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);
     }

    public void registerPatient(View v)
    {
        String drPhone = Preferences.getDrPhone(this);
        // if the device was not registered yet - refer the user to the device registration (Settings) screen
        if(drPhone == null || drPhone.equals(""))
        {
            registerDevice(v);
        }
        else {
            Intent registerPatientIntent = new Intent(this, RegisterPatientActivity.class);
            startActivity(registerPatientIntent);
        }

    }

    public void registerDevice(View v) {
        Intent registerDeviceIntent = new Intent(this, RegisterDeviceActivity.class);
        startActivity(registerDeviceIntent);
    }

    private final List blockedKeys = new ArrayList(Arrays.asList(KeyEvent.KEYCODE_VOLUME_DOWN, KeyEvent.KEYCODE_VOLUME_UP));

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (blockedKeys.contains(event.getKeyCode())) {
            return true;
        } else {
            return super.dispatchKeyEvent(event);
        }
    }



}
