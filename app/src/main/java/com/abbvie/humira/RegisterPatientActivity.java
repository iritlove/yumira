package com.abbvie.humira;

import android.content.Context;
import android.database.DataSetObserver;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.abbvie.R;
import com.abbvie.data.Patient;
import com.abbvie.utils.Constants;
import com.abbvie.utils.MailsUtil;
import com.abbvie.utils.SendMailTask;
import com.abbvie.utils.UIUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class RegisterPatientActivity extends AppCompatActivity implements TextWatcher{

    //Spinner cellPrefixSpinner;
    //ArrayAdapter<String> prefixAdapter;
    EditText patientNameEdit;
    EditText patientCellEdit;
    //String[] prefixList;
    Patient registeredPatient;
    CountDownTimer idleCountDown;
    String patientPhone, patientName;

    public static final String TAG = "TestIdle";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_patient);


        initComponents();
        addListeners();

        //prefixList = getResources().getStringArray(R.array.cellPrefix);
        //prefixAdapter = new ArrayAdapter<String>(this, R.layout.single_prefix_layout, prefixList);
        //cellPrefixSpinner.setAdapter(prefixAdapter);

        idleCountDown = new CountDownTimer(Constants.IdleTimeout, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                Log.e(TAG, "Time remaining " + (millisUntilFinished/1000));
            }

            @Override
            public void onFinish() {
                //TODO
                //Toast.makeText(RegisterPatientActivity.this, "Idle!!!", Toast.LENGTH_SHORT).show();
                finish();
            }
        };


    }

    @Override
    protected void onResume() {
        super.onResume();
        idleCountDown.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        idleCountDown.cancel();
    }

    public void initComponents()
    {
        patientNameEdit = (EditText)findViewById(R.id.patientNameEdit);
        patientCellEdit = (EditText)findViewById(R.id.patientCellEdit);
        //cellPrefixSpinner = (Spinner)findViewById(R.id.cellPrefixSpinner);

        patientCellEdit.setOnEditorActionListener(new EditText.OnEditorActionListener(){

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    v.clearFocus();

                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
                return false;

            }
        });
    }

    public void addListeners()
    {
        patientNameEdit.addTextChangedListener(this);
        patientCellEdit.addTextChangedListener(this);
        /*cellPrefixSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                restartIdleCounter();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/
    }

    public void restartIdleCounter()
    {
        idleCountDown.cancel();
        idleCountDown.start();
        Log.e(TAG, "Idle Counter is restarted");
    }



    public void registerPatient(View v)
    {

        patientName = patientNameEdit.getText().toString();
        patientPhone= patientCellEdit.getText().toString();

        if(UIUtils.isEmpty(patientName)) {
            UIUtils.showCustomDialog(this, getString(R.string.missingField, getString(R.string.patientName)), false);
            return;
        }

        if(UIUtils.isEmpty(patientPhone)) {
            UIUtils.showCustomDialog(this, getString(R.string.missingField, getString(R.string.patientPhone)), false);
            return;
        }

        if(!UIUtils.isNameValid(patientName))
        {
            UIUtils.showCustomDialog(this, getString(R.string.invalidName, getString(R.string.patientName)), false);
            return;
        }

        if(!UIUtils.isPhoneValid(patientPhone))
        {
            UIUtils.showCustomDialog(this, R.string.invalidCell, false);
            return;
        }

        if(!UIUtils.isOnline(this))
        {
            UIUtils.showCustomDialog(this, R.string.noInternetConnection, false);
            return;
        }
        idleCountDown.cancel();
        registeredPatient = new Patient(patientName,
                                        patientPhone);//,
                                        //cellPrefixSpinner.getSelectedItem().toString());
        new SendMailTask(this, registeredPatient).execute(MailsUtil.fromUserName,
                                    MailsUtil.fromPassword,
                                    MailsUtil.careForLifeEmail);
    }

    public void cancel(View v)
    {
        idleCountDown.cancel();
        finish();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        restartIdleCounter();
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    private final List blockedKeys = new ArrayList(Arrays.asList(KeyEvent.KEYCODE_VOLUME_DOWN, KeyEvent.KEYCODE_VOLUME_UP));

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (blockedKeys.contains(event.getKeyCode())) {
            return true;
        } else {
            return super.dispatchKeyEvent(event);
        }
    }
}
