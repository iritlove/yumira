package com.abbvie.humira;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.abbvie.utils.Preferences;
import com.abbvie.utils.UIUtils;

import com.abbvie.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RegisterDeviceActivity extends AppCompatActivity {

    EditText drPhoneEdit, drNameEdit, medicalCenterEdit;
    String drPhone, drName, medicalCenter;
    TextView topMessage, bottomMessage;
    TextView okButton, cancelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_device);

        initComponents();

        loadData();
    }


    public void initComponents()
    {
        drPhoneEdit = (EditText)findViewById(R.id.drPhoneEdit);
        //drPhoneEdit.setTypeface(Typeface.createFromAsset(getAssets(), "Heebo-Light.ttf"));
        drNameEdit = (EditText)findViewById(R.id.drNameEdit);
        //drNameEdit.setTypeface(Typeface.createFromAsset(getAssets(), "Heebo-Light.ttf"));
        medicalCenterEdit = (EditText)findViewById(R.id.medicalCenterEdit);
        //medicalCenterEdit.setTypeface(Typeface.createFromAsset(getAssets(), "Heebo-Light.ttf"));

        topMessage = (TextView)findViewById(R.id.titleText);
        //topMessage.setTypeface(Typeface.createFromAsset(getAssets(), "Heebo-Light.ttf"));
        bottomMessage = (TextView)findViewById(R.id.abbvieText);
        //bottomMessage.setTypeface(Typeface.createFromAsset(getAssets(), "Heebo-Light.ttf"));
        okButton = (TextView)findViewById(R.id.okButton);
        //okButton.setTypeface(Typeface.createFromAsset(getAssets(), "Heebo-Light.ttf"), Typeface.BOLD);
        cancelButton = (TextView)findViewById(R.id.cancelButton);
        //cancelButton.setTypeface(Typeface.createFromAsset(getAssets(), "Heebo-Light.ttf"));

       /* medicalCenterEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
                v.clearFocus();
                v.requestFocus();
            }
        });*/

        medicalCenterEdit.setOnEditorActionListener(new EditText.OnEditorActionListener(){

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    v.clearFocus();

                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
                return false;

            }
        });
    }

    public void loadData()
    {
        drPhone = Preferences.getDrPhone(this);
        if(drPhone == null || drPhone.equals(""))
            return;     // keep the fields empty
        drName = Preferences.getDrName(this);
        medicalCenter = Preferences.getMedicalCenter(this);

        drPhoneEdit.setText(drPhone);
        drNameEdit.setText(drName);
        medicalCenterEdit.setText(medicalCenter);
    }

    public void saveSettings(View v)
    {
        drPhone = drPhoneEdit.getText().toString();
        drName = drNameEdit.getText().toString();
        medicalCenter = medicalCenterEdit.getText().toString();

        if(UIUtils.isEmpty(drName)) {
            UIUtils.showCustomDialog(this, getString(R.string.missingField, getString(R.string.mdName)), false);
            return;
        }

        if(UIUtils.isEmpty(drPhone)) {
            UIUtils.showCustomDialog(this, getString(R.string.missingField, getString(R.string.mdCell)), false);
            return;
        }

        if(UIUtils.isEmpty(medicalCenter)) {
            UIUtils.showCustomDialog(this, getString(R.string.missingField, getString(R.string.medicalCenter)), false);
            return;
        }

        if(!UIUtils.isNameValid(drName))
        {
            UIUtils.showCustomDialog(this, getString(R.string.invalidName, getString(R.string.mdName)), false);
            return;
        }

        if(!UIUtils.isPhoneValid(drPhone))
        {
            //UIUtils.alertBox(this, R.string.invalidCell, false);
            UIUtils.showCustomDialog(this, R.string.invalidCell, false);
            return;
        }
        if(!UIUtils.isNameValid(medicalCenter))
        {
            UIUtils.showCustomDialog(this, getString(R.string.invalidName, getString(R.string.medicalCenter)), false);
            return;
        }

        Preferences.setDrName(this, drName);
        Preferences.setDrPhone(this, drPhone);
        Preferences.setMedicalCenter(this, medicalCenter);

        Toast.makeText(this, getString(R.string.settingsSavedSuccssfully), Toast.LENGTH_SHORT).show();
        finish();

    }

    public void cancel(View v)
    {
        finish();
    }

    private final List blockedKeys = new ArrayList(Arrays.asList(KeyEvent.KEYCODE_VOLUME_DOWN, KeyEvent.KEYCODE_VOLUME_UP));

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (blockedKeys.contains(event.getKeyCode())) {
            return true;
        } else {
            return super.dispatchKeyEvent(event);
        }
    }
}
